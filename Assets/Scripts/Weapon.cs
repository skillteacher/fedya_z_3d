using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private float delay = 0.2f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private GameObject sparksEffects;
    [SerializeField] private float sparksLifetime = 0.1f;
    [SerializeField] private Ammo ammo;
    private bool isReadyToShoot = true;

    private void Start()
    {
        ammo = GameObject.FindGameObjectWithTag("AmmoText").GetComponent<Ammo>();
    }

    private void Update()
    {
        if(Input.GetButtonDown("Fire1") && isReadyToShoot)
        {
            Shoot();
        }
    }

    private void Raycasting()
    {

        RaycastHit hit;
        if(!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit,range)) return;
        //Debug.Log("� ����� � " + hit.transform.name);
        SummonSparks(hit.point);
        Health targetHealth = hit.transform.GetComponent<Health>();
        if (targetHealth == null) return;
        targetHealth.TakeDamage(damage);
    }

    private void Shoot()
    {
        if (ammo.IsNoAmmo()) return;
        ammo.ReduceAmmo();
        PlayMuzzleflash();
        Raycasting();
        StartCoroutine(DelayCountdown(delay));
    }

    private void PlayMuzzleflash()
    {
        muzzleFlashEffect.Play();   
    }

    private IEnumerator DelayCountdown(float delay)
    {
        isReadyToShoot = false;
        yield return new WaitForSeconds(delay);
        isReadyToShoot = true;
    }

    private void SummonSparks(Vector3 point)
    {
        GameObject sparks = Instantiate(sparksEffects, point, Quaternion.identity);
        Destroy(sparks, sparksLifetime);
    }
   
}
