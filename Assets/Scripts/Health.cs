using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float HealthAmount = 100f;

    public void TakeDamage(float damageAmount)
    {
        HealthAmount -= damageAmount;

        if(HealthAmount <= 0)
        {
            Destroy(gameObject);
        }
    }

}
