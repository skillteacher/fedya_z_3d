using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    public event Message PlayerDeath;

    [SerializeField] private float HP = 100f;

    public void TakeDamage(float damageAmount)
    {
        HP -= damageAmount;

        if (HP <= 0)
        {
            Debug.Log("�� ����");
            PlayerDeath?.Invoke();
        }
    }
}
